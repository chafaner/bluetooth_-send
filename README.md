# 蓝牙整合语音

## 介绍
该项目主要整合了andriod 的蓝牙开发和科大讯飞的语音识别，通过语音识别得到结果后在通过蓝牙发送指令

### 目前进度

+ 蓝牙连接发送
+ 语音离线命令识别
+ 后续还会添加其他功能和更改ui界面

### 效果展示

请直接跳转看我的博客，后面ui更新后会完善Readme文档。

>[科大讯飞语音离线命令识别_茶凡_Matrix的博客-CSDN博客](https://blog.csdn.net/weixin_45833112/article/details/129885232)
>
>[andriod12（sdk33）以上整合蓝牙app_茶凡_Matrix的博客-CSDN博客](https://blog.csdn.net/weixin_45833112/article/details/129799278)
